(use-modules (guix build-system copy)
             (guix git-download)
             (guix licenses)
             (guix packages))

(define-public rocket-layout
  (let ((commit "4abbc2ad9370b4eacaf3d1ef413e22209765018a")
        (version "0.0")
        (revision "1"))
    (package
      (name "rocket-layout")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://codeberg.org/lechner/rocket-layout.git")
                      (commit commit)))
                (sha256
                 (base32
                  "02rw79vk1jk6zrd27ih8nzh470j3pzhry6ih440bk3gha1753fnz"))
                (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments (quote (#:install-plan
                         (quote (("xkb/symbols/rocket" "share/xkb/"))))))
      (synopsis "The Rocket Keyboard Layout - Supercharge Your Typing")
      (description
       "Type Control combinations with ease. Turn your Space Bar into a Control key.
Take a moment: Which key should be in the center of your keyboard? Inspired by the
brilliant Halmak, Workman, and Colemak layouts. Also has some similiarities to
Spacemacs.")
      (home-page "https://codeberg.org/lechner/rocket-layout")
      (license expat))))

rocket-layout
