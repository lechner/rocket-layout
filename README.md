The Rocket Keybord Layout - Supercharge Your Typing
===================================================

Type `Control` combinations with ease. Turns your `Space Bar` into
a `Control` key. Take a moment to think about it: Which key should
be located in the center of your keyboard?

The `Control` keys were reassigned. `Left-Ctrl` key is now `Tab`,
and `Right-Ctrl` is `Enter`. You will appreciate it. Both keys are
now at the edge of your keyboard. Navigate forms with ease, and
never again hit `Enter` by accident!

The `Caps Lock` and `Enter` keys both produce spaces. Now have two space
keys on either side—totally in line with the rocket theme!

The `Tab` key is an additional `Backspace`. That idea came from the neat
[Colemak layout](https://colemak.com/).

In a nod to programmers everywhere, Rocket also reverses the `Shift`
function on the alphanumeric digits and a few other keys. That part is
similar to the programmer's version of the
[Workman layout](https://workmanlayout.org/).
Do you also use symbols more often than digits?

The Rocket layout is otherwise based on the cool new
[Halmak layout](https://github.com/MadRabbit/halmak).
Read more about its amazing design in
[this blog post](http://nikolay.rocks/categories/optimal+keyboard).

For the best experience, you may have to take apart your keyboard. Please
do not forget to add the layout to `grub` and `initramfs`.

Please write for some tricks in the `sway` tiling Wayland compositor.
